﻿using JsGridSamples.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using JsGridSamples.Data.Interfaces;
using JsGridSamples.Data.Model;
using System.Reflection;

namespace JsGridSamples.Controllers
{
    public class SupplierController : Controller
    {
        // GET: Supplier
        IRepository _Repo;
        ModelFactory _ModelFactory;
        public SupplierController(IRepository Repo, ModelFactory ModelFactory)
        {
            _Repo = Repo;
            _ModelFactory = ModelFactory;

        }
        public ActionResult GetSuppliers()
        {
            //IEnumerable<SupplierModel> SuppliersList = _Repo.GetSuppliers().OrderBy(S => S.CompanyName).ToList().Select(S => _ModelFactory.Create(S));
            return View();
        }
        public JsonResult SearchSuppliers(int pageIndex, int pageSize, string Name, string sortField = "ContactName", string sortOrder = "asc")
        {

            int itemsCount = 0;
            IQueryable<Supplier> Query = null;
            IEnumerable<Supplier> OrderedQuery = null;

            IEnumerable<SupplierModel> SuppliersList = null;
            using (_Repo)
            {
                Query = _Repo.GetSuppliers();
                itemsCount = Query.Count();

                Type myType = typeof(Supplier);
                PropertyInfo myPropInfo = myType.GetProperty(sortField);

                if (sortOrder == "asc")
                    OrderedQuery = Query.OrderBy(S => myPropInfo.Name).ToList();
                else
                    OrderedQuery = Query.OrderByDescending(S => myPropInfo.Name).ToList();

                SuppliersList = OrderedQuery.Skip((pageIndex - 1) * pageSize).Where(S => Name == null || S.CompanyName.Contains(Name)).Take(pageSize).ToList().Select(S => _ModelFactory.Create(S));

            }
            var Result = new { data = SuppliersList, itemsCount = itemsCount };
            return Json(Result, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateSuppliers(string CompanyName)
        {
            return Json("");
        }
    }
}