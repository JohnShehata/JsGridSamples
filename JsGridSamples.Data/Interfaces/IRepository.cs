﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsGridSamples.Data.Interfaces
{
    public interface IRepository:IDisposable
    {
        IQueryable<Data.Supplier> GetSuppliers();
    }
}
