﻿using JsGridSamples.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsGridSamples.Data
{
    public class Repository:IRepository
    {
        Context _Ctx;
        public Repository(Context Ctx)
        {
            _Ctx = Ctx;
        }


        public IQueryable<Data.Supplier> GetSuppliers()
        {
            return _Ctx.Ctx.Suppliers;
        }

        public void Dispose()
        {
            if (_Ctx != null)
                _Ctx.Dispose();
        }

    }
}
