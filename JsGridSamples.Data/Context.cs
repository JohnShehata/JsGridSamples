﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsGridSamples.Data
{
    public class Context:IDisposable
    {
        NORTHWNDEntities _Ctx;
        public Context()
        {
            _Ctx = new NORTHWNDEntities();
            _Ctx.Configuration.LazyLoadingEnabled = false;
            _Ctx.Configuration.ProxyCreationEnabled = false;
        }

        public NORTHWNDEntities Ctx {
            get {
                return _Ctx;
            }
        }
        public void Dispose()
        {
            if (_Ctx != null)
                _Ctx.Dispose();

            //GC.SuppressFinalize(this);
        }

    }
}
