﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JsGridSamples.Data.Model
{
    public class ModelFactory
    {

        public SupplierModel Create(Supplier S)
        {
            return new Data.Model.SupplierModel()
            {
                Address = S.Address,
                City = S.City,
                CompanyName = S.CompanyName,
                ContactName = S.ContactName,
                ContactTitle = S.ContactTitle,
                Phone = S.Phone,
                SupplierID = S.SupplierID
            };
        }
    }
}
